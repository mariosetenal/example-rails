# Example rails

## Flujo de trabajo de Gitflow

El flujo de trabajo de Gitflow es un diseño de flujo de trabajo de Git que fue publicado por primera vez y popularizado por Vincent Driessen en nvie. El flujo de trabajo de Gitflow define un modelo estricto de ramificación diseñado alrededor de la publicación del proyecto. Proporciona un marco sólido para gestionar proyectos más grandes.

### Funcionamiento

![](.images/0.png)

#### Ramas de desarrollo y maestras

En vez de una única rama `maestra (master)`, este flujo de trabajo utiliza dos ramas para registrar el historial del proyecto. La rama `maestra (master)` almacena el historial de publicación oficial y la rama de `desarrollo (develop)` sirve como rama de integración para funciones. Asimismo, conviene etiquetar todas las confirmaciones de la rama `maestra (master)` con un `número de versión (tag)`.

#### Ramas de función

Cada nueva función debe estar en su propia rama. Sin embargo, en vez de ramificarse de la `maestra (master)`, las ramas de función utilizan la de `desarrollo (develop)` como rama primaria. Cuando una función está completa, se vuelve a fusionar en la de `desarrollo (develop)`. Las funciones nunca deben interactuar directamente con la `maestra (master)`.

![](.images/1.png)

Ten en cuenta que las ramas de función combinadas con la rama de `desarrollo (develop)` conforman, a todos efectos, el flujo de trabajo de rama de función. Sin embargo, el flujo de trabajo de Gitflow no termina aquí.

Las ramas de función normalmente se crean a partir del ultimo commit de la rama de `desarrollo (develop)`.

#### Creación de una rama de función

Se debe tener un issue para tener la referencia de los que se va a trabajar en esa rama de función. Por ejemplo se tiene el siguiente issue:

![](.images/2.png)

Los comandos que se deben ejecutar son:

```bash
$ git checkout develop
$ git pull
$ git checkout -b fix-#3
```

El nombre de la nueva rama es `fix-#3` es para hacer referencia al issue el cual se esta trabajando en esa rama. También se usa la palabra **fix** para que cuando se realice el pull request cierre el issue. Es muy importe el `#`.

Después sigue trabajando y utiliza Git como lo harías normalmente.

#### Finalización de una rama de función

Cuando hayas terminado con el trabajo de desarrollo, el siguiente paso es fusionar `feature_branch (fix-#00)` con la rama `desarrollo (develop)`.

Primero se debe generar un pull request usando la plataforma

![](.images/3.png)

Como se ve en la imagen se hace desde la rama `feature_branch (fix-#00)` hacia la rama `desarrollo (develop)`. Ademas que se marca la opción de _Close branch_ esto es para que cuando se realice el merge la rama `feature_branch (fix-#00)` se elimine.

En el apartado de _Reviewers_ se debería colocar a los usuarios que deben revisar y aprobar lo que se realizo.

##### Una vez realizado el pull request

Cuando el pull request ya este aprobado, el usuario con los permisos para hacer merge lo debe realizar.

![](.images/5.png)

El _Merge strategy_ de estar marcado como **Squash**, para que los commit se compriman en solo uno y mantenga uniforme la rama `desarrollo (develop)`. Ademas se debe marcar la opción de _Close source branch_ para que la rama se cierre.

El resultado del merge seria asi:

![](.images/6.png)

Ademas como se llamo a la rama `fix-#00` el issue se marco como resulto

![](.images/7.png)

### Ramas de publicación

![](.images/8.png)

Una vez que el desarrollo ha adquirido suficientes funciones para una publicación (o se está acercando una fecha de publicación predeterminada), bifurcas un la rama de `pruebas (test)` a partir de la rama de `desarrollo (develop)`. Dentro de la rama, no se pueden añadirse nuevas funciones tras este punto; solo las soluciones de errores, la generación de documentación y otras tareas orientadas a la publicación deben ir en la rama.

Una vez que fue probado y esté lista para el lanzamiento, la rama de publicación se fusiona en la `maestra (master)` y se etiqueta con un `número de versión (tag)`. Además, debería volver a fusionarse en la rama de `desarrollo (develop)`, que podría haber progresado desde que se inició la publicación.

Las bifurcaciones se hacen también a traves de los pull request, similar como se hizo en la rama de fusión a la rama de `desarrollo (develop)`.

![](.images/10.png)

Como se ve en la imagen se puede ver **NO** se marca la opción _Close Branch_ para que no se elimine la rama de `desarrollo (develop)`.

![](.images/11.png)

El _Merge strategy_ de estar marcado como **Squash**, para que los commit se compriman en solo uno y mantenga uniforme la rama `prueba (test)`. Ademas **NO** se debe marcar la opción de _Close source branch_ para que la no se cierre.

Estos mismos pasos se hacen de la rama `prueba (test)` a la rama de `maestra (master)`. Solo mente una vez están en la rama se hace un tag para marcar la version de despliegue.

### Ramas de corrección

![](.images/9.png)

Las ramas de mantenimiento o "corrección" (hotfix) se utilizan para reparar rápidamente las publicaciones de producción. Las ramas de corrección son muy similares a las ramas de publicación y a las de función, salvo porque se basan en la rama `maestra (master)` en vez de la de desarrollo. Es la única rama que debería bifurcarse directamente a partir de la rama `maestra (master)`. Una vez que la solución esté completa, debería fusionarse en la rama `maestra (master)` y la de desarrollo (o la rama de publicación actual), y la rama `maestra (master)` debería etiquetarse con un número de versión actualizado.

Tener una línea de desarrollo específica para la solución de errores permite que tu equipo aborde las incidencias sin interrumpir el resto del flujo de trabajo ni esperar al siguiente ciclo de publicación. Puedes considerar las ramas de mantenimiento como ramas de publicación ad hoc que trabajan directamente con la rama `maestra (master)`.

Los comandos que se deben ejecutar son:

```bash
$ git checkout master
$ git pull
$ git checkout -b hot-fix-#3
```
