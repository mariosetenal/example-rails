FROM ruby:2.6

WORKDIR /var/app

RUN apt-get update && \
    apt-get install -y sqlite3 nodejs npm && \
    npm install -g yarn

RUN gem install rails && \
    gem install bundler && \
    gem update --system